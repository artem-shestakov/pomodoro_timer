from setuptools import setup

APP = ['main.py']
DATA_FILES = ['./icons/logout.png',
              './icons/minus.png',
              './icons/pause.png',
              './icons/play-button.png',
              './icons/add.png',
              './icons/stop.png',
              './icons/tomato.png']
OPTIONS = {
    'argv_emulation': True,
    'iconfile': './icons/tomato.png',
    'plist': {
        'CFBundleShortVersionString': '0.2.0',
        'LSUIElement': True,
    },
    'packages': ['rumps'],
}

setup(
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
    install_requires=['rumps']
)
